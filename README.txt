Rio de Janeiro 09/06/2022

Ambiênte PHP com Docker

Instruções de uso

a) na raiz do projeto crie uma pasta com o nome: mysql-data
esta pasta guardará os dados enviados para o banco

b) abra o terminal e rode o comando: docker-compose up -d

c) abra o navegador no endereço localhost:8000

se tudo de certo você verá a pagina de informações do php

end points

/ = php info.
/home = página home.
/sobre = página sobre.
